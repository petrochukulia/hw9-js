// Завдання 1. Секундомір
let secondsCounter = 0, minutesCounter = 0, hoursCounter = 0, intervalHandlerSec, intervalHandlerMin, intervalHandlerHrs;

const get = id => document.getElementById(id);

const countSec = () => {
	get("sec").innerHTML = secondsCounter;
	if (secondsCounter < 59) {
		secondsCounter++;
	} else secondsCounter = 0;
}

const countMin = () => {
	get("min").innerHTML = minutesCounter;
	if (minutesCounter < 59) {
		minutesCounter++;
	} else minutesCounter = 0;
}

const countHrs = () => {
	get("hrs").innerHTML = hoursCounter;
	hoursCounter++;
}

get("start").onclick = () => {
	get("stopwatch-display").classList.add("green");
	intervalHandlerHrs = setInterval(countHrs, 60000);
	intervalHandlerMin = setInterval(countMin, 1000);
	intervalHandlerSec = setInterval(countSec, 100);
}

get("stop").onclick = () => {
	get("stopwatch-display").classList.remove("green");
	get("stopwatch-display").classList.add("red");
	clearInterval(intervalHandlerSec);
	clearInterval(intervalHandlerMin);
	clearInterval(intervalHandlerHrs);
}

get("reset").onclick = () => {
	get("stopwatch-display").classList.remove("green");
	get("stopwatch-display").classList.remove("red");
	get("stopwatch-display").classList.add("silver");
	clearInterval(intervalHandlerSec);
	clearInterval(intervalHandlerMin);
	clearInterval(intervalHandlerHrs);
	secondsCounter = 0;
	minutesCounter = 0;
	hoursCounter = 0;
	get("sec").innerText = "00";
	get("min").innerText = "00";
	get("hrs").innerText = "00";

}

// Завдання 2. Перевірка телефону 000-000-00-00
let askPhone = document.getElementById("askPhone");
let phoneField = document.createElement("input");
askPhone.after(phoneField);
let submitPhoneBtn = document.createElement("input");
submitPhoneBtn.setAttribute("type", "submit");
phoneField.after(submitPhoneBtn);


const phonePattern = /^\d{3}-\d{3}-\d{2}-\d{2}$/;

submitPhoneBtn.onclick = function(){
	if(phonePattern.test(phoneField.value)){
		phoneField.style.background = "green";
		document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
		//alert("ok")
	} else {
		let errorMsg = document.createElement("div");
		errorMsg.innerText = "Невірний формат номеру!";
		errorMsg. style.color = "red";
		phoneField.before(errorMsg);
		//alert("no")
	}
}

// Завдання 3. Слайдер

const slider = document.getElementById("slider");
const imagesId = ["img1", "img2", "img3", "img4", "img5",];
let nextImg = 0;

let showImg = () => {
	if (nextImg>=imagesId.length){
		nextImg = 0;
	}
	let item = document.getElementById(imagesId[nextImg++ % imagesId.length]);
	item.classList.add("now");
	
}
setInterval(showImg, 3000);


